const router = require('express').Router();
const passport = require('passport');
require('../../controllers/passport');
const AuthorController = require("../../controllers/authorsController");
const { validateBody, signupSchemas, loginSchemas } = require('../../helpers/routeHelpers');
const passportLocal = passport.authenticate('local', { session: false });
const passportJWT = passport.authenticate('jwt', { session: false });
const passportGoogle = passport.authenticate('googleToken', { session: false });
const passportFacebook = passport.authenticate('facebookToken', { session: false });

router.route('/signup')
  .post(validateBody(signupSchemas.authSchema), AuthorController.signup);

router.route('/login')
  .post(validateBody(loginSchemas.authSchema), passportLocal, AuthorController.login);

router.route('/oauth-google')
  .post(passportGoogle, AuthorController.login);

router.route('/oauth-facebook')
  .post(passportFacebook, AuthorController.login);

router.route('/profile')
  .get(passportJWT, AuthorController.profile);

module.exports = router;
