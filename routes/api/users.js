const router = require('express').Router();
const userController = require("../../controllers/usersController");


router.route('/register')
  .post(userController.create);

// router.route('/authors')
//   .get(userController.findByEmail);

module.exports = router;
