const router = require("express").Router();
const articleRoutes = require("./articles");
const userRoutes = require("./users");
const authorRoutes = require("./authors")

// Articles routes
router.use("/articles", articleRoutes);

// Users routes
router.use("/users", userRoutes);

// Authors Routes
router.use("/authors", authorRoutes);

module.exports = router;
