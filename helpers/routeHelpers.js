const Joi = require('joi');

module.exports = {
    validateBody: (schema) => {
        return (req, res, next) => {
            const result = Joi.validate(req.body, schema);
            if (result.error) {
                return res.status(400).json(result.error);
            }
            if (!req.value) { req.value = {}; }
            delete result.value.passwordConfirmation;
            req.value['body'] = result.value;
            next();
        }
    },

    signupSchemas: {
        authSchema: Joi.object().keys({
            email: Joi.string().email().required(),
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{7,35}$/).required(),
            passwordConfirmation: Joi.any().valid(Joi.ref('password')).required()
        })
    },

    loginSchemas: {
        authSchema: Joi.object().keys({
            email: Joi.string().email().required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{7,35}$/).required()
        })
    }
}