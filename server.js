const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const PORT = process.env.PORT || 3001;
const MONGODB_URI = process.env.MONGODB_URI;

require('./controllers/passport');

const app = express();
app.use(cors());
const routes = require("./routes");
app.use(morgan('dev'));

// Define middleware here
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Serve up static assets (usually on heroku)
if (process.env.NODE_ENV === 'production') {
  app.use(express.static("client/build"));
}
// For file upload
app.use(fileUpload());


// Define API routes here
// Add routes, both API and view
app.use(routes);

// Connect to the Mongo DB
mongoose.connect(MONGODB_URI || 'mongodb://localhost/mizzpatricia', { useNewUrlParser: true });

app.listen(PORT, () => {
  console.log(`🌎 ==> Server now on port ${PORT}!`);
});
