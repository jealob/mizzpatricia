const Services = require('../services');

Services.mailService.sendMail({
  subject: 'Test email ' + new Date().toString(),
  to: 'jealob@hotmail.com',
  text: `This is plaintext
  
  This is another plaintext paragraph
  
Best,
The Support Team at mizzpatricia
`,
  html: `<p>This is HTML Text</p>
<p><a href="https://google.com">HTML para with link</a></p>
<p>Best,<br /><strong>The Support Team</strong> at mizzpatricia</p>`
}).catch((err) => {
  console.error(err);
  process.exit();
});
