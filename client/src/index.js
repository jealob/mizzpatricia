import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import axios from 'axios';

import './styles/css/index.css';
import App from './App';
import registerServiceWorker from "./registerServiceWorker";
import Home from './containers/Home';
import SignUp from './containers/SignUp';
import SignIn from './containers/Login';
import LogOut from './containers/LogOut';
import Dashboard from './containers/Dashboard';
import NoMatch from './containers/NoMatch';
import reducers from './reducers';
import Authenticate from './containers/Authenticate';
require('dotenv').config();

const jwtToken = localStorage.getItem('JWT_TOKEN');
axios.defaults.headers.common['Authorization'] = jwtToken; //Default behavior for all axios request

ReactDOM.render(
    <Provider store={createStore(reducers,
        {
            auth: {
                token: jwtToken,
                isAuthenticated: jwtToken ? true : false,
            }
        },
        compose(
            applyMiddleware(reduxThunk),
            // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
        ))}>
        <BrowserRouter>
            <App>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/login" component={SignIn} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route exact path="/logout" component={LogOut} />
                    <Route exact path="/dashboard" component={Authenticate(Dashboard)} />
                    <Route component={NoMatch} />
                </Switch>
            </App>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();;
