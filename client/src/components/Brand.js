import React from "react";

export const Brand = () => {
    return (
      <div className=" mb-4 text-center">
        <h4 className="display-6">Miss Patricia's Evolution</h4>
      </div>
    )
  }

  export const Footer = () => {
    return (
      <footer className="footer text-center">
        <div className="bottom">
          &copy; MizzPatricia
        </div>
      </footer >
    )
  }
  