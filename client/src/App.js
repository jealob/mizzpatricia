import React from 'react';
import Header from './containers/Header';
import { Brand, Footer } from './components/Brand'

export default (props) => {
    return (
        <div>
            <Header />
            <div className="container wrapper">
                <Brand />
                {props.children}
            </div>
            <Footer />
        </div>
    )
}