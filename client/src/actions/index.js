import axios from 'axios';

const baseUrl = process.env.REACT_APP_DEV_API_URL;

export const signUp = (data) => {
    return async (dispatch) => {
        try {
            const res = await axios.post(baseUrl + '/authors/signup', data)
            dispatch({
                type: "LOGIN",
                payload: res.data.author
            });
            localStorage.setItem('JWT_TOKEN', res.data.author.token);
            axios.defaults.headers.common['Authorization'] = res.data.author.token;
        }
        catch (error) {
            if (error.message.includes('403')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "User already exist"
                });
            }
            else if (error.message.includes('400')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "Invalid signup information"
                });
            }
        };
    }
}

export const oauthGoogle = (data) => {
    return async (dispatch) => {
        try {
            const res = await axios.post(baseUrl + '/authors/oauth-google', { access_token: data });
            dispatch({
                type: "LOGIN",
                payload: res.data.author
            });
            localStorage.setItem('JWT_TOKEN', res.data.author.token);
            axios.defaults.headers.common['Authorization'] = res.data.author.token;
        }
        catch (error) {
            if (error.message.includes('403')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "User already exist"
                });
            }
            else if (error.message.includes('400')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "Invalid signup information"
                });
            }
        };
    }
}

export const oauthFacebook = (data) => {
    return async (dispatch) => {
        try {
            const res = await axios.post(baseUrl + '/authors/oauth-facebook', { access_token: data });
            dispatch({
                type: "LOGIN",
                payload: res.data.author
            });
            localStorage.setItem('JWT_TOKEN', res.data.author.token);
            axios.defaults.headers.common['Authorization'] = res.data.author.token;
        }
        catch (error) {
            if (error.message.includes('403')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "User already exist"
                });
            }
            else if (error.message.includes('400')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "Invalid signup information"
                });
            }
        };
    }
}

export const login = (data) => {
    return async (dispatch) => {
        try {
            const res = await axios.post(baseUrl + '/authors/login', data);
            dispatch({
                type: "LOGIN",
                payload: res.data.author
            });
            localStorage.setItem('JWT_TOKEN', res.data.author.token);
            axios.defaults.headers.common['Authorization'] = res.data.author.token;
        }
        catch (error) {
            if (error.message.includes('400') || error.message.includes('401')) {
                dispatch({
                    type: 'AUTH_ERROR',
                    payload: "Invalid email and password combination"
                });
            }
        };
    }
}

export const logOut = () => {
    return function (dispatch) {
        localStorage.removeItem('JWT_TOKEN');
        axios.defaults.headers.common['Authorization'] = '';
        dispatch({
            type: 'LOG_OUT',
            payload: ''
        });
    }
}

export const loadAuthor = () => {
    return function (dispatch) {
        axios.get(`${baseUrl}/authors/profile`)
            .then(function (res) {
                dispatch({
                    type: 'LOAD_AUTHOR',
                    payload: res.data
                });
            })
            .catch(function (error) {
                dispatch({
                    type: 'LOAD_AUTHOR_ERROR',
                    payload: 'Error loading profile'
                });
            })
    }
}

export const fetchArticles = () => {
    return function (dispatch) {
        axios.get(baseUrl + '/articles/all')
            .then(function (res) {
                dispatch({
                    type: 'LOAD_ARTICLES',
                    payload: res.data
                });
            })
            .catch(function (error) {
                dispatch({
                    type: 'LOAD_ARTICLES_ERROR',
                    payload: 'Error fetching post'
                });
            })
    }
}
