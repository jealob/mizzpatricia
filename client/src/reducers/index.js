import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './auth';
import postReducer from './post';
import authorReducer from './author';

export default combineReducers({
    form: formReducer,
    auth: authReducer,
    post: postReducer,
    author: authorReducer
})