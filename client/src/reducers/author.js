const initialState = {
    profile: {},
    errorMessage: ''
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_AUTHOR':
            return { ...state, profile: action.payload, errorMessage: '' }
        case 'LOAD_AUTHOR_ERROR':
            return { ...state, errorMessage: action.payload }
        default:
            return state
    }
}