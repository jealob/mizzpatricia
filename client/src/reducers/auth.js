const initialState = {
    isAuthenticated: false,
    token:'',
    errorMessage: '',
}
export default (state = initialState, action) => {  
    switch (action.type) {
        case 'LOGIN':
            return { ...state, token:action.payload.token, profile: action.payload.profile, isAuthenticated: true, errorMessage: '' }
        case 'LOG_OUT':
            return { ...state, token:'', profile: '', isAuthenticated: false, errorMessage: '' }
        case 'AUTH_ERROR':
            return { ...state, token:'', isAuthenticated: false, errorMessage: action.payload}
        default:
            return state
    }
}