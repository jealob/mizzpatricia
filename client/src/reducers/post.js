const initialState = {
    articles: [],
    errorMessage: ''
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_ARTICLES':
            return { ...state, articles: action.payload }
        case 'LOAD_ARTICLES_ERROR':
            return { ...state, errorMessage: action.payload}
        default:
            return state
    }
}