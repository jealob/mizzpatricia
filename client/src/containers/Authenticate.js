import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions'; 

export default (OriginalComponent) => {
    class MixedComponent extends Component {
        authChecker() {
            if (!this.props.isAuthenticated && !this.props.jwtToken) {
                this.props.history.push('/login');
            }
        }
        componentWillMount() {
            this.authChecker();
        }

        componentWillUpdate() {
            this.authChecker();
        }

        render() {
            return <OriginalComponent {...this.props} />
        }
    }

    function mapStateToProps(state) {
        return {
            isAuthenticated: state.auth.isAuthenticated,
            jwtToken: state.auth.token,
        }
    }
    return connect(mapStateToProps, actions)(MixedComponent);
};