import React, { Component } from "react";

class SideBar extends Component {

    render() {
        const styling = {
            height: "100%",
            padding: "0.5em"
        };
        return (
            <div className="border" style={styling}>
                SideBar Content
           </div>
        );
    }
}

export default SideBar;