import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../actions';

class Header extends Component {
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
    }
    logOut() {
        this.props.logOut();
    }
    componentDidMount() {
        this.props.loadAuthor();
    }
    render() {
        const { first_name, profile_photo } = this.props.author.profile;
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark header">
                    <Link className="navbar-brand" to="/">MizzPatricia</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link header-link" to="/dashboard">Dashboard</Link>
                            </li>
                        </ul>
                        {!this.props.isAuthenticated ?
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <Link className="nav-link header-link" to="/signup">Sign Up</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link header-link" to="/login">Login</Link>
                                </li>
                            </ul>
                            :
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <img className="nav-link profile-thumbnail" src={profile_photo} alt={first_name} />
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link header-link" to="/logout" onClick={this.logOut}>Log Out</Link>
                                </li>
                            </ul>
                        }
                    </div>
                </nav>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        author: state.author
    }
}

export default connect(mapStateToProps, actions)(Header);