import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { compose } from 'redux';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';


import * as actions from '../actions';
import CustomInput from '../components/CustomInput';

class Login extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.responseFacebook = this.responseFacebook.bind(this);
    this.responseGoogle = this.responseGoogle.bind(this);
  }

  redirect() {
    if (!this.props.errorMessage) {
      this.props.history.push('/dashboard');
    }
  }

  onSubmit(formData) {
    this.props.login(formData)
      .then(() => this.redirect())
      .catch((error) => console.error(error));
  }

  responseGoogle(res) {
    this.props.oauthGoogle(res.accessToken)
      .then(() => this.redirect())
      .catch((error) => console.error(error));
  }

  responseFacebook(res) {
    this.props.oauthFacebook(res.accessToken)
      .then(() => this.redirect())
      .catch((error) => console.error(error));
  }

  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.redirect();
    }
  }

  required = value => value ? undefined : 'Required';
  email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined;

  render() {
    const googleID = process.env.REACT_APP_GOOGLE_CLIENT_ID;
    const facebookID = process.env.REACT_APP_FACEBOOK_CLIENT_ID;
    const { handleSubmit } = this.props;
    return (
      <div className="container text-center login-form">
        <p className="border rounded p-2">
          Sign in using any of your social media account or with your email.
        </p>
        {this.props.errorMessage ?
          <div className="alert alert-warning">
            {this.props.errorMessage}
          </div>
          : null}
        <div className="text-center">
          <GoogleLogin
            clientId={googleID}
            render={renderProps => (
              <button className="btn btn-outline-danger btn-block my-1" onClick={renderProps.onClick}>Sign in with Google</button>
            )}
            onSuccess={this.responseGoogle}
            onFailure={this.responseGoogle}
          />
          <FacebookLogin
            appId={facebookID}
            textButton="Sign in with Facebook"
            fields="name,email,picture"
            callback={this.responseFacebook}
            cssClass="btn btn-outline-primary btn-block my-1"
          />
        </div>
        <form className="mt-4" onSubmit={handleSubmit(this.onSubmit)}>
          <fieldset>
            <Field
              name="email"
              type="email"
              id="email"
              placeholder="Your email"
              component={CustomInput}
              validate={[this.email]}
            />
          </fieldset>
          <fieldset>
            <Field
              name="password"
              type="password"
              id="password"
              placeholder="Enter your password"
              component={CustomInput}
              validate={[this.required]}
            />
          </fieldset>
          <div className='mb-3'>
            <button type="submit" className="btn btn-primary btn-block">Log in</button>
          </div>
          <div >
            <Link to='/forgot-password'><small>Forgot Password?</small></Link>
          </div>
          <div >
            <Link to="/signup"><small>Don't have an account? Sign up</small></Link>
          </div>
        </form>
      </div >
    )
  }
};

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.errorMessage,
    isAuthenticated: state.auth.isAuthenticated
  }
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'signup' })
)(Login);

