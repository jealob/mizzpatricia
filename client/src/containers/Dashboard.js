import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: this.props.auth.email,
        }
    }
    componentDidMount() {
        this.props.loadAuthor();
    }
    render() {
        const { email, first_name, last_name, profile_photo } = this.props.author.profile;
        return (
            <div className="row">
                <div className="col-sm-12 p-2 text-center profile">
                    <img className="profile-photo" src={profile_photo} alt={first_name} />
                    <div>
                        Welcome!<br /> {first_name} {last_name}
                    </div>
                    <div>
                        {email}
                    </div>
                </div>
                {/* <div className="col-sm-9 text-center ml-auto post">
                    Coming Soon
                    </div> */}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth,
        author: state.author
    }
}
export default connect(mapStateToProps, actions)(Dashboard);