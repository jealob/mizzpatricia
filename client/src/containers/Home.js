import React, { Component } from "react";
import { connect } from 'react-redux';

import { fetchArticles } from '../actions';

const moment = require('moment');

class Home extends Component {

    componentDidMount() {
        this.props.fetchArticles();
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    {!this.props.errorMessage ?
                        (<div>
                            {this.props.articles.map(article => (
                                <div className="post" key={article.id}>
                                    <div className="text-center p-2 m-2">
                                        <h4>{article.title}</h4>
                                    </div>
                                    <div className="border rounded p-2 mx-2 mb-3">
                                        {article.synopsis}
                                    </div>
                                    <div className=" mx-2 author-info">
                                        posted: {moment(article.date, "YYYYMMDD").fromNow()}<br/>
                                        {article.author}
                                    </div>
                                </div>
                            ))}
                        </div>) : (this.props.errorMessage)}
                </div>
                {/* <div className="col-sm-3 commercial">
                    Commercial
                </div> */}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        articles: state.post.articles,
        errorMessage: state.post.message
    }
}

export default connect(mapStateToProps, { fetchArticles })(Home);