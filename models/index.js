module.exports = {
  Article: require('./article'),
  User: require('./user'),
  Author: require('./author')
};
