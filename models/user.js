const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
    email: String,
    username: String,
    password: String,
}, {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });
const User = mongoose.model('user', userSchema);
module.exports = User;

// Hash password
module.exports.hashPassword = async (password) => {
    try {
        const salt = await bcrypt.genSalt(10);
        return await bcrypt.hash(password, salt);
    }
    catch (error) {
        throw new Error('Hashing failed', error);
    }
}

// Compare login password with that in the database
module.exports.comparePassword = async (inputPassword, hashedPassword) => {
    try {
        return await bcrypt.compare(inputPassword, hashedPassword);
    }
    catch (error) {
        throw new Error('Comparing failed', error);
    }
}