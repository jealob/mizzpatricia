const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const authorSchema = new Schema({
    method: {
        type: String,
        enum: ['local', 'google', 'facebook'],
        required: true
    },
    email: {
        type: String,
        lowercase: true
    },
    first_name:String,
    last_name:String,
    profile_photo: String,
    local: {
        password: {
            type: String
        },
        secretToken: String,
        verified: Boolean,
        // timestamps: {
        //     createdAt: 'created_at',
        //     updatedAt: 'updatedAt',
        // }
    },
    google: {
        id: {
            type: String
        }
    },
    facebook: {
        id: {
            type: String
        }
    }
});

authorSchema.pre('save', async function (next) {
    try {
        if (this.method !== 'local') {
            next();
        }
        const salt = await bcrypt.genSalt(10);
        const passwordHash = await bcrypt.hash(this.local.password, salt);
        this.local.password = passwordHash;
    }
    catch (error) {
        next(error);
    }
});

authorSchema.methods.isValidPassword = async function (newPassword) {
    try {
        return await bcrypt.compare(newPassword, this.local.password);
    }
    catch (error) {
        throw new Error(error);
    }
}
const Author = mongoose.model('author', authorSchema);
module.exports = Author;