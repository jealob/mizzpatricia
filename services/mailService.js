const SENDGRID_API_KEY = process.env.SENDGRID_API;
const sendgrid = require('@sendgrid/mail');
const mailConfig = require('../config/mailerConfig');

// sendgrid.setApiKey(SENDGRID_API_KEY);
sendgrid.setApiKey('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');
function sendMail(options) {
  const mailOptions = {
    fromEmail: (options.fromEmail) ? options.fromEmail : mailConfig.fromEmail,
    fromName: (options.fromName) ? options.fromName : mailConfig.fromName,
    to: options.to,
    subject: options.subject,
    text: options.text,
    html: mailConfig.emailLayout(options.html)
  };

  return sendgrid.send({
    to: mailOptions.to,
    from: { email: mailOptions.fromEmail, name: mailOptions.fromName },
    subject: mailOptions.subject,
    text: mailOptions.text,
    ...(mailOptions.html ? { html: mailOptions.html } : {})
  })
    .catch((err) => {
      console.error(err.toString());
      return false;
    })
    .then(() => true);
}

module.exports = {
  sendMail
};
