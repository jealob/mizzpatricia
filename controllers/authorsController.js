const JWT = require('jsonwebtoken');
require('dotenv').config();
const Author = require("../models/author");

signAuthToken = author => {
  const JWT_AUTH_SECRET = process.env.JWT_AUTH_SECRET;
  return JWT.sign({
    iss: 'mizzpatricia',
    sub: author._id,
    iat: new Date().getTime(),
    exp: Math.floor(Date.now() / 1000) + (60 * 5)
  }, JWT_AUTH_SECRET);
}

// Defining methods for the articlesController
module.exports = {
  signup: async (req, res, next) => {
    // Get Validated author data from Joi
    const { email, firstName, lastName, password } = req.value.body;
    // Check if email is already taken
    Author
      .findOne({ 'email': email })
      .then(dbModel => {
        if (dbModel) {
          return res.status(403).json({ report: "taken" });
        }
        Author
          .create({
            method: 'local',
            email: email,
            first_name: firstName,
            last_name: lastName,
            profile_photo: "https://ui-avatars.com/api/?name=" + firstName + "+" + lastName,
            local: {
              password: password,
              verified: false
            }
          })
          .then(newAuthor => {
            // Generate Token
            // here return author profile along with token
            // Currently just returning just the token
            const author = {
              token: signAuthToken(newAuthor),
              profile: newAuthor
            }
            res.status(200).json({ author });
          })
          .catch(err => res.status(422).json(err))
      })
      .catch(err => res.status(422).json(err))
  },
  login: async (req, res, next) => {
    // Generate Token
    // here return author profile along with token
    // Currently just returning just the token
    try {
      const author = {
        token: await signAuthToken(req.user),
        profile: req.user
      }
      res.status(200).json({ author });
    }
    catch (error) {
      return done(error, false, error.message);
    }
  },

  profile: async (req, res) => {
    try {
      return res.status(200).json(req.user);
    }
    catch (error) {
      return res.status(422).json(error);
    }
  }
};
