const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const FacebookTokenStrategy = require('passport-facebook-token');
const { ExtractJwt } = require('passport-jwt');
require('dotenv').config();

const Author = require('../models/author');
const JWT_AUTH_SECRET = process.env.JWT_AUTH_SECRET;
const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
const FACEBOOK_CLIENT_ID = process.env.FACEBOOK_CLIENT_ID
const FACEBOOK_CLIENT_SECRET = process.env.FACEBOOK_CLIENT_SECRET
// JWT Strategy 
// Call passport middleware using the jsonwebtoken strategy
passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'), //where the token is coming form
    secretOrKey: JWT_AUTH_SECRET //The secret
}, async (payload, done) => {
    try {
        const author = await Author.findById(payload.sub);
        if (!author) {
            return done(null, false);
        }
        return done(null, author);
    }
    catch (error) {
        return done(error, false);
    }
}));

// Authenticate user
passport.use('local', new LocalStrategy({
    usernameField: 'email'
}, async (email, password, done) => {
    try {
        // 1. check if email already exist
        const author = await Author.findOne({ 'email': email });
        if (!author) {
            return done(null, false);
        }
        // 2. check if the password is correct
        const isValid = await author.isValidPassword(password);
        if (isValid) {
            return done(null, author);
        }
        else {
            return done(null, false);
        }
    }
    catch (error) {
        return done(error, false);
    }
}));

passport.use('googleToken', new GooglePlusTokenStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
}, async (accessToken, refreshToken, profile, done) => {
    try {
        // Check whether this current usesr exist in our DB
        const authorExist = await Author.findOne({ "email": profile.emails[0].value });
        if (authorExist) {
            if (!authorExist.google.id)
                await authorExist.update({ $set: { "google.id": profile.id } });
            return done(null, authorExist);
        }
        // Creates a new user
        const newAuthor = new Author({
            method: 'google',
            email: profile.emails[0].value,
            first_name: profile.name.givenName,
            last_name: profile.name.familyName,
            profile_photo: profile.photos[0].value,
            google: {
                id: profile.id
            }
        });
        await newAuthor.save();
        done(null, newAuthor);
    }
    catch (error) {
        return done(error, false, error.message);
    }
}));

// Facebook OAuth Stratedy
passport.use('facebookToken', new FacebookTokenStrategy({
    clientID: FACEBOOK_CLIENT_ID,
    clientSecret: FACEBOOK_CLIENT_SECRET
}, async (accessToken, refreshToken, profile, done) => {
    // Check whether this current usesr exist in our DB
    try {
        const authorExist = await Author.findOne({ "email": profile.emails[0].value });
        if (authorExist) {
            if (!authorExist.facebook.id)
                await authorExist.update({ $set: { "facebook.id": profile.id } });
            return done(null, authorExist);
        }
        // Creates a new user
        const newAuthor = new Author({
            method: 'facebook',
            email: profile.emails[0].value,
            first_name: profile.name.givenName,
            last_name: profile.name.familyName,
            profile_photo: profile.photos[0].value,
            facebook: {
                id: profile.id
            }
        });

        await newAuthor.save();
        done(null, newAuthor);

    } catch (error) {
        return done(error, false, error.message);
    }
}));