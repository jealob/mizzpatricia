const db = require("../models");
const Joi = require('joi');

// Defining methods for the articlesController
module.exports = {
  create: function (req, res) {
    // Validation of user data (Joi)
    const userSchema = Joi.object().keys({
      email: Joi.string().email().required(),
      username: Joi.string().required(),
      password: Joi.string().regex(/^[a-zA-Z0-9]{7,35}$/).required(),
      passwordConfirmation: Joi.any().valid(Joi.ref('password')).required()
    });


    const result = Joi.validate(req.body, userSchema);
    if (result.error) {
      // req.flash('error', 'Data not valid');
      // res.redirect('/users/register');
      return;
    }

    // Check if email is already taken
    db.User
      .findOne({ 'email': result.value.email })
      .then(dbModel => {
        if (dbModel) {
          emailIsNotTaken = false;
          res.send(emailIsNotTaken);
          return;
        }

        // Hash Password
        db.User.hashPassword(result.value.password)
          .then(hash => {
            delete result.value.passwordConfirmation;
            result.value.password = hash;

            // Create New Account in database
            db.User
              .create(result.value)
              .then(dbModel => {
                if (dbModel) {
                  emailIsNotTaken = true;
                  res.send(emailIsNotTaken);
                }
              })
              .catch(err => res.status(422).json(err));
          });
      })
      .catch(err => res.status(422).json(err));
  }
};
