module.exports = {
  fromEmail: 'info@mizzpatricia.com',
  fromName: 'mizzpatricia',
  emailLayout: function (html) {
    return `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Demystifying Email Design</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
        .footer {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 14px;
        color: #ffffff;
        }
        </style>
        </head>
        <body style="margin: 0; padding: 0;">
        <table align="center" cellpadding="0" cellspacing="0" width="600" style = "border-collapse: collapse">
        <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 10px 0 10px 0;">
        <a href="https://www.mizzpatricia.com" style="border:0"; text-decoration:none;">
        <img src="https://storage.googleapis.com/enview-public-general/images/logo-teal-orange-email.png" title = "Enview Logo" alt="enview_logo" height="auto" width="33%" style="display:block;">
        </a>
        </td>
        </tr>
        <tr>
        <td bgcolor="#ffffff" style="padding: 10px 10px 10px 10px;">
        ${html}
        </td>
        </tr>
        <tr height="86">
        <td class = "footer" align="center" bgcolor="#176881" style="padding: 10px 0 10px 0;">
        <div>mizzpatricia Blog &copy; 2018</div>
        <a style="color: #ffffff;"><font color="#ffffff">support@mizzpatricia.com</font></a> 
        </td>
        </tr>
        </table>
        </body>
        </html>
        `}
};
