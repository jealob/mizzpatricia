# mizzpatricia
Install dependencies for mizzpatricia app with:

`npm install`

Next seed database:

`node scripts/seedDB.js`

Follow code to add the necessary .env files both on server and client level

Run the mizzpatricia app with:

`npm start`